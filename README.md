# Desafio Ioasys

#### 🚀 Technologies, libs and frameworks:

- [Javascript](https://www.javascript.com/) \- Programming language;
- [ReactJS](https://reactjs.org/) \- The web framework used;
- [Styled Components](https://styled-components.com/) \- Utilising tagged template literals and the power of CSS;
- [CSS Pure](https://developer.mozilla.org/en-US/docs/Web/CSS) \- Cascading Style Sheets \(CSS\) is a style sheet language;
- [ESLint](https://eslint.org/) \- Find and fix problems in your JavaScript code;

---

#### 🔧 How to run the application?

At the terminal, clone the project:

```
git clone git@bitbucket.org:aliceaatalla/desafio-ioasys.git
```

Enter the project folder:

```
cd desafio-ioasys
```

Install the dependencies:

```
npm install
```

Run the application:

```
npm start
```

There, you can now access the application from the route http://localhost:3000/
