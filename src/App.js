import React from 'react';
import styled from 'styled-components';

import AppRoutes from './AppRoutes';

const AppWrapper = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`;

const App = () => (
  <AppWrapper>
    <AppRoutes />
  </AppWrapper>
);

export default App;
