import React from 'react';
import { ThemeProvider } from 'styled-components';

const px = (size) => `${size}px`;

const typography = {
  fontWeightLight: 300,
  fontWeightNormal: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  fontSizeXXL: px(36),
  fontSizeXL: px(32),
  fontSizeL: px(28),
  fontSizeML: px(24),
  fontSizeM: px(20),
  fontSizeSM: px(18),
  fontSizeS: px(16),
  fontSizeXS: px(14),
  fontSizeXXS: px(12),
  fontSizeXXXS: px(10),
};

const palette = {
  white: '#FFFFFF',
  darkGrey: '#333333',
  greyLight: '#999999',
  black: '#000000',
  purple: '#B22E6F',
  purpleLight: '#AB2680',
};

const theme = {
  palette,
  typography,
};

const Theme = ({ children }) => <ThemeProvider theme={theme}>{children}</ThemeProvider>;

export default Theme;
