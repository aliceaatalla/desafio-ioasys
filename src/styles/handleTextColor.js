const handleTextColor = (theme, textColor) => {
  switch (textColor) {
    case 'white':
      return theme.palette.white;
    case 'darkGrey':
      return theme.palette.darkGrey;
    default:
      return theme.palette.white;
  }
};

export default handleTextColor;
