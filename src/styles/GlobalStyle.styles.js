import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900&display=swap');

  ${normalize}


  body {
    font-family: 'Heebo', sans-serif;
  }
`;

export default GlobalStyle;
