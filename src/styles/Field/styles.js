import styled from 'styled-components';

export const Label = styled.label`
  font-weight: ${({ theme }) => theme.typography.fontWeightNormal};
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  color: ${({ theme }) => theme.palette.white};
  padding: 8px 13px 4px;
  position: absolute;
  opacity: 0.7;
`;

export const Input = styled.input`
  font-weight: ${({ theme }) => theme.typography.fontWeightNormal};
  font-size: ${({ theme }) => theme.typography.fontSizeS};
  color: ${({ theme }) => theme.palette.white};
  background-color: transparent;
  box-sizing: border-box;
  padding: 28px 13px 8px;
  height: 60px;
  width: 100%;
  border: none;
  outline: 0;
`;

export const WrapperInput = styled.div`
  background: ${({ theme }) => theme.palette.black};
  position: relative;
  border-radius: 4px;
  margin-top: 16px;
  height: 60px;
  opacity: 0.3;
  width: 100%;
`;
