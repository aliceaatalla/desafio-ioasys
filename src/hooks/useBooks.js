import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import getBooks from '../api/get-books';

const useBooks = () => {
  const navigate = useNavigate();
  const [books, setBooks] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalBooksPages, setTotalBooksPages] = useState(0);

  const fetchBooks = useCallback(async () => {
    const { data, totalPages, status } = await getBooks(pageNumber);
    setBooks(data);
    setTotalBooksPages(totalPages);

    if (status === 401) {
      localStorage.removeItem('token');
      navigate('/');
    }
  }, [pageNumber]);

  return {
    books,
    fetchBooks,
    pageNumber,
    setPageNumber,
    totalBooksPages,
  };
};

export default useBooks;
