import { useState } from 'react';
import { getToken } from '../api/client';

const useToken = () => {
  const initialToken = getToken();

  const [token, setToken] = useState(initialToken || '');

  const saveToken = (userToken) => {
    window.localStorage.setItem('token', userToken);

    setToken(token);
  };

  return {
    setToken: saveToken,
    token,
  };
};

export default useToken;
