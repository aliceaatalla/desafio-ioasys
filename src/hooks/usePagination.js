import { useState } from 'react';

const usePagination = ({ pageNumber, setPageNumber, totalBooksPages }) => {
  const [disableNextButton, setDisableNextButton] = useState(false);
  const [disablePrevButton, setDisablePrevButton] = useState(true);
  const firstPage = pageNumber < 1;
  const lastPage = pageNumber === totalBooksPages - 1;

  const handleNextPage = () => {
    setPageNumber(pageNumber + 1);

    if (lastPage) {
      setDisableNextButton(true);
    }
    if (!firstPage && !lastPage) {
      setDisablePrevButton(false);
      setDisableNextButton(false);
    }
  };

  const handlePrevPage = () => {
    setPageNumber(pageNumber - 1);

    if (firstPage) {
      setDisablePrevButton(true);
    }
    if (!lastPage && !firstPage) {
      setDisableNextButton(false);
      setDisablePrevButton(false);
    }
  };

  return {
    disableNextButton,
    disablePrevButton,
    handleNextPage,
    handlePrevPage,
  };
};

export default usePagination;
