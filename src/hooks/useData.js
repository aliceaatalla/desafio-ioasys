import { useState } from 'react';

const useData = () => {
  const initialData = {};
  const [data, setData] = useState(initialData);

  return {
    setData,
    data,
  };
};

export default useData;
