import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import getBookDetails from '../api/get-book-details';

const useBookDetails = () => {
  const [bookDetails, setBookDetails] = useState([]);
  const navigate = useNavigate();

  const fetchBookDetails = async (bookId) => {
    const { data, status } = await getBookDetails(bookId);
    setBookDetails(data);

    if (status === 401) {
      localStorage.removeItem('token');
      navigate('/');
    }
  };

  return {
    bookDetails,
    fetchBookDetails,
  };
};

export default useBookDetails;
