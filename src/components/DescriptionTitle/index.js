import styled from 'styled-components';

const DescriptionTitle = styled.span`
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightMedium};
  color: ${({ theme }) => theme.palette.darkGrey};
  text-transform: ${({ uppercase }) => (uppercase ? 'uppercase' : 'capitalize')};
  line-height: 20px;
  margin: 0;
`;

export default DescriptionTitle;
