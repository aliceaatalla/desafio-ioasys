import React from 'react';
import { Container, Text } from './styles';

const TextPagination = ({ currentPage, totalPages }) => (
  <Container>
    <Text>
      Página
      <Text strong>{currentPage}</Text>
      de
      <Text strong>{totalPages}</Text>
    </Text>
  </Container>
);

export default TextPagination;
