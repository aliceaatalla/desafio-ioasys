import styled from 'styled-components';

export const Container = styled.p``;

export const Text = styled.span`
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme, strong }) =>
    strong ? theme.typography.fontWeightMedium : theme.typography.fontWeightNormal};
  color: ${({ theme }) => theme.palette.darkGrey};
  margin: 0 3px;
`;
