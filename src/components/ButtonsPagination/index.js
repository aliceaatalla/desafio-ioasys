import React from 'react';
import {
  Container,
  PrevButtonDisabled,
  NextButtonDisabled,
  PrevButtonEnable,
  NextButtonEnable,
  Button,
} from './styles';

const ButtonsPagination = ({
  disableNextButton,
  disablePrevButton,
  handleNextPage,
  handlePrevPage,
}) => (
  <Container>
    <Button type="button" onClick={handlePrevPage} disabled={disablePrevButton}>
      {disablePrevButton ? <PrevButtonDisabled /> : <PrevButtonEnable />}
    </Button>
    <Button type="button" onClick={handleNextPage} disabled={disableNextButton}>
      {disableNextButton ? <NextButtonDisabled /> : <NextButtonEnable />}
    </Button>
  </Container>
);

export default ButtonsPagination;
