import styled from 'styled-components';
import PaginationButtonDisabledSVG from '../../assets/Images/PaginationButtonDisabledSVG.svg';
import PaginationButtonEnableSVG from '../../assets/Images/PaginationButtonEnableSVG.svg';

export const PrevButtonDisabled = styled.img.attrs({
  src: PaginationButtonDisabledSVG,
  alt: 'Botão para voltar para página anterior desabilitado',
})``;

export const NextButtonDisabled = styled.img.attrs({
  src: PaginationButtonDisabledSVG,
  alt: 'Botão para avançar de página desabilitado',
})`
  transform: rotate(180deg);
`;

export const PrevButtonEnable = styled.img.attrs({
  src: PaginationButtonEnableSVG,
  alt: 'Botão para voltar para página anterior habilitado',
})`
  transform: rotate(180deg);
`;

export const NextButtonEnable = styled.img.attrs({
  src: PaginationButtonEnableSVG,
  alt: 'Botão para avançar de página habilitado',
})``;

export const Button = styled.button`
  background-color: transparent;
  outline: none;
  border: none;
  cursor: pointer;
`;

export const Container = styled.div``;
