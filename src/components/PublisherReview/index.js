import React from 'react';

import DescriptionTitle from '../DescriptionTitle';

import { Container, Review } from './styles';

const PublisherReview = ({ description }) => (
  <Container>
    <DescriptionTitle uppercase>resenha da editora</DescriptionTitle>

    <Review>{description}</Review>
  </Container>
);

export default PublisherReview;
