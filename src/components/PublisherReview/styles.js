import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 24px 24px;
`;

export const Review = styled.q`
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightNormal};
  color: ${({ theme }) => theme.palette.darkGrey};
  line-height: 20px;
  padding-top: 20px;
`;
