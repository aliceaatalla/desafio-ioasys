import styled from 'styled-components';

export const Image = styled.img`
  width: 240px;
  height: 351px;
  border-radius: 0px;
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightMedium};
  color: ${({ theme }) => theme.palette.darkGrey};

  @media (min-width: 900px) {
    width: 349px;
    height: 512.29px;
  }
`;

export const Wrapper = styled.div`
  width: 240px;
  height: 351px;
  padding: 24px;

  @media (min-width: 900px) {
    padding: 30px;
    width: 349px;
    height: 512.29px;
  }
`;
