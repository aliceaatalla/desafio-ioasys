import styled from 'styled-components';

const Description = styled.span`
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightNormal};
  color: ${({ theme }) => theme.palette.greyLight};
  line-height: 20px;
`;

export default Description;
