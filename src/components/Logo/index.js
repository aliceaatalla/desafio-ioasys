import styled from 'styled-components';
import handleTextColor from '../../styles/handleTextColor';

const Logo = styled.h1`
  font-size: ${({ theme }) => theme.typography.fontSizeXXL};
  font-weight: ${({ theme }) => theme.typography.fontWeightBold};
  color: ${({ theme, textColor }) => handleTextColor(theme, textColor)};
`;

export default Logo;
