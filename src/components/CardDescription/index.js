import React from 'react';

import AuthorName from '../AuthorName';
import BookTitle from '../BookTitle';
import Description from '../Description';
import { Container, Wrapper } from './styles';

const CardDescription = ({
  authorsNames,
  bookTitle,
  numberOfPages,
  publishingCompany,
  publicationDate,
}) => (
  <Container>
    <Wrapper>
      <BookTitle>{bookTitle}</BookTitle>
      {authorsNames &&
        authorsNames.map((authorName) => <AuthorName key={authorName}>{authorName}</AuthorName>)}
    </Wrapper>
    <Wrapper>
      <Description>{numberOfPages} páginas</Description>
      <Description>{publishingCompany}</Description>
      <Description>Publicado em {publicationDate}</Description>
    </Wrapper>
  </Container>
);

export default CardDescription;
