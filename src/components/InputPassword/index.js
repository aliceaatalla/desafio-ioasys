import React from 'react';
import { Input, Label, WrapperInput } from '../../styles/Field/styles';

const InputPassword = ({ password, handleChangePassword }) => (
  <WrapperInput>
    <Label htmlFor="inputPassword">Senha</Label>
    <Input id="inputPassword" type="password" value={password} onChange={handleChangePassword} />
  </WrapperInput>
);

export default InputPassword;
