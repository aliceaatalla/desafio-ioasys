import styled from 'styled-components';

const BookTitle = styled.h2`
  font-size: ${({ theme, isModal }) =>
    isModal ? theme.typography.fontSizeL : theme.typography.fontSizeXS};
  font-weight: ${({ theme, isModal }) =>
    isModal ? theme.typography.fontWeightMedium : theme.typography.fontWeightMedium};
  color: ${({ theme }) => theme.palette.darkGrey};
  line-height: ${({ isModal }) => (isModal ? '40px' : '20px')};
  margin: 0;
`;

export default BookTitle;
