import styled from 'styled-components';
import handleTextColor from '../../styles/handleTextColor';

const AppTitle = styled.h2`
  font-size: ${({ theme }) => theme.typography.fontSizeL};
  font-weight: ${({ theme }) => theme.typography.fontWeightLight};
  color: ${({ theme, textColor }) => handleTextColor(theme, textColor)};
`;

export default AppTitle;
