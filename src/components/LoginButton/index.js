import styled from 'styled-components';

export const LoginButton = styled.button`
  font-weight: ${({ theme }) => theme.typography.fontWeightMedium};
  font-size: ${({ theme }) => theme.typography.fontSizeS};
  background-color: ${({ theme }) => theme.palette.white};
  color: ${({ theme }) => theme.palette.purple};
  position: absolute;
  border-radius: 44px;
  margin: 12px;
  bottom: 0;
  right: 10px;
  width: 85px;
  height: 36px;
  cursor: pointer;
  border: none;
`;
