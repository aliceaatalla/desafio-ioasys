import styled from 'styled-components';
import CloseIconSVG from '../../assets/Images/CloseIconSVG.svg';

export const Image = styled.img.attrs({ src: CloseIconSVG, alt: 'Botão de deslogar' })``;

export const Button = styled.button`
  margin-right: 16px;
  align-self: flex-end;
  background-color: transparent;
  outline: none;
  border: none;
  cursor: pointer;

  @media (min-width: 900px) {
    margin-right: 124px;
  }
`;
