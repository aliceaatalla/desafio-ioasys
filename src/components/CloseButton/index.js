import React from 'react';

import { Image, Button } from './styles';

const CloseButton = ({ handleClick }) => (
  <Button type="button" onClick={handleClick}>
    <Image />
  </Button>
);

export default CloseButton;
