import React from 'react';

import LibraryTitle from '../LibraryTitle';
import Logout from '../Logout';
import { Container } from './styles';

const Header = () => (
  <Container>
    <LibraryTitle textColor="darkGrey" />
    <Logout />
  </Container>
);

export default Header;
