import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 42px 16px;

  @media (min-width: 600px) {
    margin: 0 114px;
  }
`;
