import styled from 'styled-components';

export const Container = styled.div`
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  margin: 0;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;

  padding: 80px 0;

  @media (max-width: 900px) {
    padding: 40px 0;
  }
`;

export const Modal = styled.div`
  background-color: ${({ theme }) => theme.palette.white};
  position: relative;
  border-radius: 4px;
  margin: 64px 16px;
  max-height: calc(100vh - 210px);
  overflow-y: auto;
  align-self: center;

  @media (min-width: 900px) {
    display: flex;
    width: 769px;
    padding: 24px;
    height: 610px;
  }
`;

export const Wrapper = styled.div`
  @media (min-width: 900px) {
    padding: 5px;
  }
`;
