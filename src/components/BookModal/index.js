import React from 'react';
import CloseButton from '../CloseButton';

import ModalImage from '../ModalImage';
import ModalText from '../ModalText';
import PublisherReview from '../PublisherReview';
import { Container, Modal, Wrapper } from './styles';

const BookModal = ({ bookDetails, handleCloseModal }) => {
  console.log('bookDetails', bookDetails);
  console.log('bookDetails', bookDetails.title);
  return (
    <Container>
      <CloseButton type="button" handleClick={handleCloseModal} />
      <Modal>
        <ModalImage bookImage={bookDetails.imageUrl} bookTitle={bookDetails.title} />
        <Wrapper>
          <ModalText
            bookTitle={bookDetails.title}
            authorsNames={bookDetails.authors}
            title={bookDetails.title}
            publisher={bookDetails.publisher}
            pageCount={bookDetails.pageCount}
            published={bookDetails.published}
            language={bookDetails.language}
            isbn10={bookDetails.isbn10}
            isbn13={bookDetails.isbn13}
          />
          <PublisherReview description={bookDetails.description} />
        </Wrapper>
      </Modal>
    </Container>
  );
};

export default BookModal;
