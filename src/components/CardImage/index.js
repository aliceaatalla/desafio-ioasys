import React from 'react';

import { Image, Wrapper } from './styles';

const CardImage = ({ bookImage, bookTitle }) => (
  <Wrapper>
    <Image src={bookImage} alt={`Imagem do livro ${bookTitle}`} />
  </Wrapper>
);

export default CardImage;
