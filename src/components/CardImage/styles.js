import styled from 'styled-components';

export const Image = styled.img`
  height: 122px;
  width: 81px;
  border-radius: 0px;
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightMedium};
  color: ${({ theme }) => theme.palette.darkGrey};
`;

export const Wrapper = styled.div`
  height: 122px;
  width: 81px;
`;
