import React from 'react';
import Description from '../Description';

import DescriptionTitle from '../DescriptionTitle';

import { Container, WrapperRight, WrapperLeft } from './styles';

const ModalInfo = ({ title, publisher, pageCount, published, language, isbn10, isbn13 }) => (
  <Container>
    <WrapperLeft>
      <DescriptionTitle uppercase>informações</DescriptionTitle>
      <DescriptionTitle>páginas</DescriptionTitle>
      <DescriptionTitle>editora</DescriptionTitle>
      <DescriptionTitle>publicação</DescriptionTitle>
      <DescriptionTitle>idioma</DescriptionTitle>
      <DescriptionTitle>título original</DescriptionTitle>
      <DescriptionTitle uppercase>isbn-10</DescriptionTitle>
      <DescriptionTitle uppercase>isbn-13</DescriptionTitle>
    </WrapperLeft>

    <WrapperRight>
      <Description>{pageCount}</Description>
      <Description>{publisher}</Description>
      <Description>{published}</Description>
      <Description>{language}</Description>
      <Description>{title}</Description>
      <Description>{isbn10}</Description>
      <Description>{isbn13}</Description>
    </WrapperRight>
  </Container>
);

export default ModalInfo;
