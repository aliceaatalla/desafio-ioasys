import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;
export const Wrapper = styled.p`
  display: flex;
  flex-direction: column;
`;

export const WrapperLeft = styled(Wrapper)`
  align-items: flex-start;
`;

export const WrapperRight = styled(Wrapper)`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;
