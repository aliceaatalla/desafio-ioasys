import React from 'react';
import AuthorName from '../AuthorName';
import BookTitle from '../BookTitle';
import ModalInfo from '../ModalInfo';

import { Container } from './styles';

const ModalText = ({
  bookTitle,
  authorsNames,
  title,
  publisher,
  pageCount,
  published,
  language,
  isbn10,
  isbn13,
}) => (
  <Container>
    <BookTitle isModal>{bookTitle}</BookTitle>
    {authorsNames &&
      authorsNames.map((authorName) => <AuthorName key={authorName}>{authorName}</AuthorName>)}
    <ModalInfo
      title={title}
      publisher={publisher}
      pageCount={pageCount}
      published={published}
      language={language}
      isbn10={isbn10}
      isbn13={isbn13}
    />
  </Container>
);

export default ModalText;
