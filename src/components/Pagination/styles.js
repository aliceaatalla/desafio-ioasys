import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  @media (min-width: 600px) {
    justify-content: flex-end;
    margin-right: 123px;
  }
`;
