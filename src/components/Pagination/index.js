import React from 'react';
import usePagination from '../../hooks/usePagination';

import ButtonsPagination from '../ButtonsPagination';
import TextPagination from '../TextPagination';
import { Container } from './styles';

const Pagination = ({ pageNumber, setPageNumber, totalBooksPages }) => {
  const { disableNextButton, disablePrevButton, handleNextPage, handlePrevPage } = usePagination({
    pageNumber,
    setPageNumber,
    totalBooksPages,
  });

  return (
    <Container>
      <TextPagination currentPage={pageNumber} totalPages={totalBooksPages} />
      <ButtonsPagination
        disableNextButton={disableNextButton}
        disablePrevButton={disablePrevButton}
        handleNextPage={handleNextPage}
        handlePrevPage={handlePrevPage}
      />
    </Container>
  );
};

export default Pagination;
