import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.palette.white};
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
  max-width: 288px;
  border-radius: 4px;
  flex: 1 1 280px;
  height: 160px;
  margin: 16px 20px 0 0;
  cursor: pointer;

  @media (min-width: 500px) {
    margin: 16px;
  }
`;

export const Button = styled.button`
  all: unset;
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
`;
