import React from 'react';

import Card from '../Card';
import { Button, Container } from './styles';

const BookCard = ({
  bookId,
  authorsNames,
  bookTitle,
  bookImage,
  numberOfPages,
  publishingCompany,
  publicationDate,
  handleCardClick,
}) => (
  <Container>
    <Button type="button" onClick={() => handleCardClick(bookId)}>
      <Card
        authorsNames={authorsNames}
        bookImage={bookImage}
        bookTitle={bookTitle}
        numberOfPages={numberOfPages}
        publishingCompany={publishingCompany}
        publicationDate={publicationDate}
      />
    </Button>
  </Container>
);

export default BookCard;
