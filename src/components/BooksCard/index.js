import React from 'react';
import BookCard from '../BookCard';
import { Container } from './styles';

const BooksCard = ({ books, handleCardClick }) => (
  <Container>
    {books &&
      books.map((book) => (
        <BookCard
          bookId={book.id}
          key={book.id}
          authorsNames={book.authors}
          bookImage={book.imageUrl}
          bookTitle={book.title}
          numberOfPages={book.pageCount}
          publishingCompany={book.publisher}
          publicationDate={book.published}
          handleCardClick={handleCardClick}
        />
      ))}
  </Container>
);

export default BooksCard;
