import React from 'react';
import { Input, Label, WrapperInput } from '../../styles/Field/styles';

const InputEmail = ({ email, handleChangeEmail }) => (
  <WrapperInput>
    <Label htmlFor="inputEmail">Email</Label>
    <Input id="inputEmail" type="email" value={email} onChange={handleChangeEmail} />
  </WrapperInput>
);

export default InputEmail;
