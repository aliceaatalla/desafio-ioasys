import React from 'react';
import AppTitle from '../AppTitle';
import Logo from '../Logo';
import { Wrapper } from './styles';

const LibraryTitle = ({ textColor }) => (
  <Wrapper>
    <Logo textColor={textColor}>ioasys</Logo>
    <AppTitle textColor={textColor}>Books</AppTitle>
  </Wrapper>
);

export default LibraryTitle;
