import React from 'react';

import CardDescription from '../CardDescription';
import CardImage from '../CardImage';
import { Container } from './styles';

const Card = ({
  authorsNames,
  bookTitle,
  bookImage,
  numberOfPages,
  publishingCompany,
  publicationDate,
}) => (
  <Container>
    <CardImage bookImage={bookImage} bookTitle={bookTitle} />
    <CardDescription
      authorsNames={authorsNames}
      bookTitle={bookTitle}
      numberOfPages={numberOfPages}
      publishingCompany={publishingCompany}
      publicationDate={publicationDate}
    />
  </Container>
);

export default Card;
