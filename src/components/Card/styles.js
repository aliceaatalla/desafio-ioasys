import styled from 'styled-components';

export const Container = styled.div`
  padding: 20px 16px;
  box-sizing: border-box;
  display: flex;
`;
