import styled from 'styled-components';
import LogoutSVG from '../../assets/Images/LogoutSVG.svg';

export const Image = styled.img.attrs({ src: LogoutSVG, alt: 'Botão de deslogar' })``;

export const Button = styled.button`
  background-color: transparent;
  outline: none;
  border: none;
  cursor: pointer;
`;
