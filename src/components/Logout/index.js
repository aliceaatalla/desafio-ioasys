import React from 'react';

import { Image, Button } from './styles';

const Logout = () => {
  const handleClick = () => {
    localStorage.removeItem('token');
  };

  return (
    <Button type="button" onClick={handleClick}>
      <Image />
    </Button>
  );
};

export default Logout;
