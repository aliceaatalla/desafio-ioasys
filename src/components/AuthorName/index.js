import styled from 'styled-components';

const AuthorName = styled.h3`
  font-size: ${({ theme }) => theme.typography.fontSizeXXS};
  font-weight: ${({ theme }) => theme.typography.fontWeightNormal};
  color: ${({ theme }) => theme.palette.purpleLight};
  margin: 0;
`;

export default AuthorName;
