import Api from './client';

const getBookDetails = async (bookId) => {
  try {
    const response = await Api.get(`/books/${bookId}`);
    const data = await response.data;

    return { data };
  } catch (error) {
    return { status: error.response.status };
  }
};

export default getBookDetails;
