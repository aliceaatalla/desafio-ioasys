import axios from 'axios';

export const getToken = () => window.localStorage.getItem('token');

const Api = axios.create({
  baseURL: process.env.REACT_APP_IOASYS_DOMAIN,
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
  },
  timeout: 10000,
});

Api.interceptors.request.use((config) => {
  const accessToken = getToken();
  if (accessToken) config.headers.Authorization = `Bearer ${accessToken}`;
  return config;
});

export default Api;
