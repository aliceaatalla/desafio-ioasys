import Api from './client';

const getBooks = async (pageNumber) => {
  try {
    const response = await Api.get(`/books?page=${pageNumber}&amount=10`);
    const data = await response.data;

    return data;
  } catch (error) {
    return { status: error.response.status };
  }
};

export default getBooks;
