import Api from './client';

const signIn = async ({ email, password }) => {
  try {
    const response = await Api.post('/auth/sign-in', { email, password });
    const headers = await response.headers;
    const data = await response.data;

    return { authorization: headers.authorization, data };
  } catch (error) {
    return { status: error.response.status };
  }
};

export default signIn;
