import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import BookModal from '../../components/BookModal';

import BooksCard from '../../components/BooksCard';
import Header from '../../components/Header';
import Pagination from '../../components/Pagination';
import useBookDetails from '../../hooks/useBookDetails';
import useBooks from '../../hooks/useBooks';

import useToken from '../../hooks/useToken';
import { Container } from './styles';

const Home = () => {
  const { token } = useToken();
  const navigate = useNavigate();
  const [isOpenModal, setIsOpenModal] = useState(false);

  const { books, fetchBooks, pageNumber, setPageNumber, totalBooksPages } = useBooks();
  const { bookDetails, fetchBookDetails } = useBookDetails();

  useEffect(() => {
    if (!token) {
      navigate('/');
    }
  });

  useEffect(() => {
    fetchBooks();
  }, [pageNumber, fetchBooks]);

  const toogleModal = () => {
    setIsOpenModal(!isOpenModal);
  };

  const handleCardClick = (bookId) => {
    fetchBookDetails(bookId);
    toogleModal();
  };

  return (
    <Container>
      <Header />
      <BooksCard books={books} handleCardClick={handleCardClick} />
      <Pagination
        pageNumber={pageNumber}
        setPageNumber={setPageNumber}
        totalBooksPages={totalBooksPages}
      />
      {isOpenModal && <BookModal bookDetails={bookDetails} handleCloseModal={toogleModal} />}
    </Container>
  );
};

export default Home;
