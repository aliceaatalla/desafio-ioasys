import styled from 'styled-components';
import HomeBackground1SVG from '../../assets/Images/HomeBackground1SVG.svg';
import HomeBackground2SVG from '../../assets/Images/HomeBackground2SVG.svg';

export const Container = styled.div`
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  width: 100vw;
  height: 100vh;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(${HomeBackground1SVG}), url(${HomeBackground2SVG});
  background-blend-mode: darken;

  display: flex;
  flex-direction: column;
`;
