import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import useData from '../../hooks/useData';
import useToken from '../../hooks/useToken';

import signIn from '../../api/sign-in';
import InputEmail from '../../components/InputEmail';
import InputPassword from '../../components/InputPassword';
import LibraryTitle from '../../components/LibraryTitle';
import { LoginButton } from '../../components/LoginButton';
import { Container, Form, Wrapper } from './styles';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const { setData } = useData();
  const { token, setToken } = useToken();

  useEffect(() => {
    if (token) {
      navigate('/home');
    }
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { authorization, data } = await signIn({
      email,
      password,
    });

    if (authorization) {
      navigate('/home');
      setToken(authorization);
      setData(data);
    }
  };

  const handleChange = (event, setValue) => setValue(event.target.value);

  return (
    <Container>
      <Wrapper>
        <LibraryTitle textColor="white" />
        <Form onSubmit={handleSubmit}>
          <InputEmail handleChangeEmail={(event) => handleChange(event, setEmail)} />
          <InputPassword handleChangePassword={(event) => handleChange(event, setPassword)} />
          <LoginButton type="submit">Entrar</LoginButton>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;
