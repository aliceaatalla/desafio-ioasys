import styled from 'styled-components';
import LoginBackgroundSVG from '../../assets/Images/LoginBackgroundSVG.svg';

export const Container = styled.div`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: absolute;
  width: 100vw;
  height: 100vh;
  background-size: cover;
  background-image: url(${LoginBackgroundSVG});
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;

  @media (min-width: 600px) {
    display: flex;
    justify-content: flex-start;
  }
`;

export const Wrapper = styled.div`
  margin: auto 16px;
  @media (min-width: 600px) {
    margin-left: 115px;
  }
`;

export const Form = styled.form`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: relative;
  width: 288px;

  @media (min-width: 600px) {
    width: 368px;
  }
`;
